const data = {
  menu: [
    { type: '主廚的話1', title: '簡介檢ˋ界點業界至簡介', link: 'javascript:void();'},
    { type: '主廚的話1', title: '123456', link: 'javascript:void();'},
    { type: '主廚的話2', title: '簡介檢ˋ界至簡介', link: 'javascript:void();'},
    { type: '主廚的話3', title: '簡介檢業界至簡介', link: 'javascript:void();'},
    { type: '主廚的話4', title: '簡介檢界至簡介', link: 'javascript:void();'},
    { type: '主廚的話5', title: '簡介界至簡介', link: 'javascript:void();'}
  ],
  input: {
    type: '全部',
    title: ''
  }
}

new Vue({
  el: '#app',
  data: data,
  computed: {
    typeMenu(){
      if(this.input.type !== '全部'){
        return this.menu.filter(item => {
          return item.type === this.input.type
        })
      } else {
        return this.menu
      }
    },
    titleMenu(){
      if(this.input.title){
        return this.typeMenu.filter(item => {
          const content = item.title.toLowerCase();
          const keyword = this.input.title.toLowerCase();
          return content.indexOf(keyword) !== -1
        })
      }else{
        return this.typeMenu
      }
    }
  }
})