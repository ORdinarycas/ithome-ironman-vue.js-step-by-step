const data = {
  input: {
    type: '主廚的話1',
    title: ''
  },
  menu: [
    { type: '主廚的話1', title: '簡介檢ˋ界點業界至簡介', link: 'javascript:void();'},
    { type: '主廚的話2', title: '簡介檢ˋ界至簡介', link: 'javascript:void();'},
    { type: '主廚的話3', title: '簡介檢業界至簡介', link: 'javascript:void();'},
    { type: '主廚的話4', title: '簡介檢界至簡介', link: 'javascript:void();'},
    { type: '主廚的話5', title: '簡介界至簡介', link: 'javascript:void();'}
  ]
}

let vm = new Vue({
  el: '#app',
  data: data,
  methods: {
    inputHandler() {
      if (this.input.title) {
        // 寫法一
        // 參考資料 https://eyesofkids.gitbooks.io/javascript-start-from-es6/content/part4/destructuring.html
        let { type, title } = this.input;
        this.menu.push({type,title,link: 'javascript:void();'})
        // 寫法二
        // this.menu.push({
        //   type: this.input.type,
        //   title: this.input.title,
        //   link: 'javascript:void();'
        // })
        this.input.title = '';
      }
    }
  }
})