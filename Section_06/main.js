const data = {
  menu: [
    { type: '主廚的話1', title: '簡介檢ˋ界點業界至簡介', link: 'javascript:void();'},
    { type: '主廚的話1', title: '123456', link: 'javascript:void();'},
    { type: '主廚的話2', title: '簡介檢ˋ界至簡介', link: 'javascript:void();'},
    { type: '主廚的話3', title: '簡介檢業界至簡介', link: 'javascript:void();'},
    { type: '主廚的話4', title: '簡介檢界至簡介', link: 'javascript:void();'},
    { type: '主廚的話5', title: '簡介界至簡介', link: 'javascript:void();'}
  ],
  input: {
    type: null,
    title: null
  }
}

new Vue({
  el: '#app',
  data: data,
  computed: {
    typeList(){
      let obj = {
        sort: [],
        map: {}
      }
      this.menu.forEach(({ type,title,link}, index) => {
        if(!obj.map[type]){
          obj.sort.push(type)
          obj.map[type] = {
            sort: [],
            map: {}
          }
        }
        obj.map[type].sort.push(title)
        obj.map[type].map[title] = {index,link}
      })
      return obj
    },
    titleList(){
      this.input.title = null;
      if(this.input.type){
        return this.typeList.map[this.input.type]
      }else{
        return []
      }
    },
    content(){
      if(this.input.title){
        return this.titleList.map[this.input.title]
      }else{
        return null
      }
    }
  }
})